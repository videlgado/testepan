Aplicação para Teste do Pan

Segue Caminhos por Cenários.

Cenário 1: /cliente/getCliente/{cpf}
Verbo: get

Cenário 2: /cliente/getCep/{cep}
Verbo: get

Cenário 3: /cliente/getEstado
Verbo: get

Cenário 4: /getMunicipios/codUF/{uf}
/getMunicipio/{cpf} (Busca municipios de acordo com o código UF do cliente salvo na base)
Verbo: get (ambos)

Cenário 5: /mudaendereco/{cpf}
Param: Endereco
Verbo: Patch


Database: H2
