package br.com.pan.teste.testepan.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Microrregiao {
	 private float id;
	 private String nome;
	 Mesorregiao MesorregiaoObject;


	 // Getter Methods 

	 public float getId() {
	  return id;
	 }

	 public String getNome() {
	  return nome;
	 }

	 public Mesorregiao getMesorregiao() {
	  return MesorregiaoObject;
	 }

	 // Setter Methods 

	 public void setId(float id) {
	  this.id = id;
	 }

	 public void setNome(String nome) {
	  this.nome = nome;
	 }

	 public void setMesorregiao(Mesorregiao mesorregiaoObject) {
	  this.MesorregiaoObject = mesorregiaoObject;
	 }
	}