package br.com.pan.teste.testepan.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.pan.teste.testepan.model.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente,String> {

	public Optional<Cliente> findByCpf(String cpf);

	public void save(Optional<Cliente> cliente);
}
