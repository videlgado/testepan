package br.com.pan.teste.testepan.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Cliente {

	@Id
	@GeneratedValue
	private Integer ID;
	
	@Column(nullable=false)
	private String Nome;
	
	@Column(nullable=false)
	private String cpf;
	
	@Column(nullable=false)
	private String Endereco;
	
	@Column(nullable=false)
	private String cep;
	
	@Column(nullable=false)
	private String Estado;
	
	@Column(nullable=false)
	private Integer IdUF;

	@Column(nullable=false)
	private String Municipio;
	
	public void setCPF(String CPF) {
		this.cpf = CPF;
	}
	public String getCPF() {
		return this.cpf;
	}
	public void setEndereco(String Endereco) {
		this.Endereco = Endereco;
	}
	public String getEndereco() {
		return this.Endereco;
	}
	public void setCEP(String CEP) {
		this.cep = CEP;
	}
	public String getCEP() {
		return this.cep;
	}
	public void setUF(Integer UF) {
		this.IdUF = UF;
	}
	public Integer getUF() {
		return this.IdUF;
	}
	public void setMunicipio(String Municipio) {
		this.Municipio = Municipio;
	}
	public String getMunicipio() {
		return this.Municipio;
	}
	public void setEstado(String Estado) {
		this.Estado = Estado;
	}
	public String getEstado() {
		return this.Estado;
	}
	public void setNome(String Nome) {
		this.Nome = Nome;
	}
	public String getNome() {
		return this.Nome;
	}
}
