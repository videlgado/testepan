package br.com.pan.teste.testepan.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Municipio {
	private float id;
	 private String nome;
	 Microrregiao MicrorregiaoObject;


	 // Getter Methods 

	 public float getId() {
	  return id;
	 }

	 public String getNome() {
	  return nome;
	 }

	 public Microrregiao getMicrorregiao() {
	  return MicrorregiaoObject;
	 }

	 // Setter Methods 

	 public void setId(float id) {
	  this.id = id;
	 }

	 public void setNome(String nome) {
	  this.nome = nome;
	 }

	 public void setMicrorregiao(Microrregiao microrregiaoObject) {
	  this.MicrorregiaoObject = microrregiaoObject;
	 }
	}