package br.com.pan.teste.testepan.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Mesorregiao {
	 private float id;
	 private String nome;
	 UF UFObject;


	 // Getter Methods 

	 public float getId() {
	  return id;
	 }

	 public String getNome() {
	  return nome;
	 }

	 public UF getUF() {
	  return UFObject;
	 }

	 // Setter Methods 

	 public void setId(float id) {
	  this.id = id;
	 }

	 public void setNome(String nome) {
	  this.nome = nome;
	 }

	 public void setUF(UF UFObject) {
	  this.UFObject = UFObject;
	 }
	}