package br.com.pan.teste.testepan.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import br.com.pan.teste.testepan.pojo.Estado;

@Component
public class EstadoService {
	
	@Value("${url.estados}")
	private String UrlEstado;
	
	public List<Estado>chamarConsultaAPIEstado(){

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> httpEntity = new HttpEntity<String>("parameters", requestHeaders);
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
	    ResponseEntity<List<Estado>> exchange = restTemplate.exchange(this.UrlEstado, HttpMethod.GET, httpEntity, new ParameterizedTypeReference<List<Estado>>(){});
	    return exchange.getBody().stream()
	            .sorted((p1, p2) -> p1.getNome().compareTo(p2.getNome()))
	            .collect(Collectors.toList());
	}
}
