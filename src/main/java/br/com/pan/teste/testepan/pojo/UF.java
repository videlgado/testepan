package br.com.pan.teste.testepan.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UF {
	 private float id;
	 private String sigla;
	 private String nome;
	 Regiao RegiaoObject;


	 // Getter Methods 

	 public float getId() {
	  return id;
	 }

	 public String getSigla() {
	  return sigla;
	 }

	 public String getNome() {
	  return nome;
	 }

	 public Regiao getRegiao() {
	  return RegiaoObject;
	 }

	 // Setter Methods 

	 public void setId(float id) {
	  this.id = id;
	 }

	 public void setSigla(String sigla) {
	  this.sigla = sigla;
	 }

	 public void setNome(String nome) {
	  this.nome = nome;
	 }

	 public void setRegiao(Regiao regiaoObject) {
	  this.RegiaoObject = regiaoObject;
	 }
	}