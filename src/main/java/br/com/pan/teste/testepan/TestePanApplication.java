package br.com.pan.teste.testepan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class TestePanApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestePanApplication.class, args);
	}

}
