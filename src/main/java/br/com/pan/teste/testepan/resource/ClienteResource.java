package br.com.pan.teste.testepan.resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import br.com.pan.teste.testepan.model.Cliente;
import br.com.pan.teste.testepan.pojo.Cep;
import br.com.pan.teste.testepan.pojo.Estado;
import br.com.pan.teste.testepan.pojo.Municipio;
import br.com.pan.teste.testepan.repository.ClienteRepository;
import br.com.pan.teste.testepan.service.ClienteService;
import br.com.pan.teste.testepan.service.EstadoService;
import br.com.pan.teste.testepan.service.MunicipioService;



@RestController
@RequestMapping("/cliente")
public class ClienteResource {
	
	private final ClienteRepository repository;
	
	@Autowired
	private EstadoService estado;
	
	@Autowired
	private MunicipioService municipio;
	
	@Autowired
	private ClienteService cliente;
	
	@Value("${url.estados}")
	private String UrlEstado;
	@Value("${url.cep}")
	private String UrlCep;
	@Value("${url.estado.municipios}")
	private String UrlMunicipios;
	
	ClienteResource(ClienteRepository repository){
		this.repository = repository;
	}
	
	@GetMapping
	public List<Cliente> listar(){
		return repository.findAll();
	}
	@GetMapping(value="/getCliente/{cpf}")
	public ResponseEntity<Cliente> buscarClientePorCPF(@PathVariable(name="cpf")String cpf){
		
		return cliente.buscarCliente(cpf);
		
	}
	
	@GetMapping(value="/getCep/{cep}")
	public ResponseEntity<Cep> buscarCep(@PathVariable(name="cep")String cep){
		
	    RestTemplate restTemplate = new RestTemplate();
	    String uri = this.UrlCep;
	    Map<String, String> params = new HashMap<String, String>();
	    params.put("cep", cep);
	    Cep cepojo = restTemplate.getForObject(uri, Cep.class, params);
	    
	    return new ResponseEntity<Cep>(cepojo, HttpStatus.OK);
	}
	
	@GetMapping(value="/getEstado")
	public List<Estado> buscarEstado(){
		return estado.chamarConsultaAPIEstado();
	}
	
	@GetMapping(value="/getMunicipio/{cpf}")
	public ResponseEntity<List<Municipio>> getMunicipio(@PathVariable(name="cpf")String cpf){
		int uf = cliente.buscaClienteRetornaUF(cpf);
		return municipio.chamarConsultaAPIMunicipioPorUF(uf);
	}
	
	@GetMapping(value="/getMunicipios/codUF/{uf}")
	public ResponseEntity<List<Municipio>> getMunicipioPorCodUF(@PathVariable(name="uf")int uf){
		return municipio.chamarConsultaAPIMunicipioPorUF(uf);
	}
	
	@PatchMapping(value="/mudaendereco/{cpf}")
	public ResponseEntity<?> changeAddress(@PathVariable(name="cpf")String cpf, @RequestParam("Endereco") String Endereco){
		return cliente.atualizarEnderecoCliente(Endereco,cpf);
		
	}
}
