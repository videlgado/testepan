package br.com.pan.teste.testepan.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import br.com.pan.teste.testepan.model.Cliente;
import br.com.pan.teste.testepan.repository.ClienteRepository;

@Component
public class ClienteService {

	@Autowired
	private ClienteRepository repository;
	
	public ResponseEntity<Cliente> buscarCliente(String cpf){
		Optional<Cliente> cliente = repository.findByCpf(cpf);
		return cliente.isPresent() ? ResponseEntity.ok(cliente.get()) : ResponseEntity.notFound().build();
	}
	
	public Integer buscaClienteRetornaUF(String cpf) {
		ResponseEntity<Cliente> entity = buscarCliente(cpf);
		return entity.getBody().getUF().intValue();
	}
	public ResponseEntity<?> atualizarEnderecoCliente(String Endereco, String cpf){

		Optional<Cliente> cliente = repository.findByCpf(cpf);
		if(cliente.isPresent()) {
			cliente.get().setEndereco(Endereco);
			repository.save(cliente);
			return ResponseEntity.ok("Endereço Atualizado");
		}else {
			return (ResponseEntity<?>) ResponseEntity.notFound();
		}
	}
}
