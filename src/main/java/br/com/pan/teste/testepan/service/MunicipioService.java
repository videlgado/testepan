package br.com.pan.teste.testepan.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.pan.teste.testepan.pojo.Municipio;

@Component
public class MunicipioService {
	@Value("${url.estado.municipios}")
	private String UrlMunicipio;
	
	private String UrlMunicipioUF = "https://servicodados.ibge.gov.br/api/v1/localidades/estados/{UF}/municipios";
	
	public ResponseEntity<List<Municipio>> chamarConsultaAPIMunicipio(Integer id){

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> httpEntity = new HttpEntity<String>("parameters", requestHeaders);
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
		Map<String, Integer> params = new HashMap<String, Integer>();
		params.put("id", id);
	    ResponseEntity<List<Municipio>> exchange = restTemplate.exchange(this.UrlMunicipio, HttpMethod.GET, httpEntity, new ParameterizedTypeReference<List<Municipio>>(){}, params);
	    return exchange;
	}
	public ResponseEntity<List<Municipio>> chamarConsultaAPIMunicipioPorUF(Integer UF){
		
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> httpEntity = new HttpEntity<String>("parameters", requestHeaders);
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
		Map<String, Integer> params = new HashMap<String, Integer>();
		params.put("UF", UF);
		ResponseEntity<List<Municipio>> exchange = null;

		UriComponentsBuilder uri = UriComponentsBuilder.fromHttpUrl(this.UrlMunicipioUF).queryParam("UF", UF);
		exchange = restTemplate.exchange(this.UrlMunicipioUF, HttpMethod.GET, httpEntity, new ParameterizedTypeReference<List<Municipio>>(){}, params);
	    return exchange;
	}
	
	//https://servicodados.ibge.gov.br/api/v1/localidades/estados/{UF}/municipios

}
